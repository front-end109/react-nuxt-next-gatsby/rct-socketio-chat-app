import express from 'express';
import http  from 'http';
import cors from 'cors';
import { Server } from 'socket.io';

const app = express();
app.use(cors());

const server = http.createServer(app);
const PORT = 4000 || process.env.PORT  ;
const cors_config = {cors: { origin:"localhost:3000", 
                            methods: ["GET", "POST"]},};

const io = new Server(server, cors_config);

// io events
io.on("connection", (sock) => {
    console.log(sock.id, "has connected.");

    sock.on("join_room", (data) => {
        sock.join(data);
        console.log(`User with ID: ${sock.id} joined room: ${data}`);
      });
    
      sock.on("send_message", (data) => {
        sock.to(data.room).emit("receive_message", data);
      });
    
    sock.on("disconnect", ()=> {
        console.log(sock.id , " user has disconnected!" );
    });
});


// app.get('/', (req, res) => {
//   res.send('<h1>Hello world</h1>');
// });


server.listen(PORT , () => {
    console.log(`listening on ${PORT}...`);
});